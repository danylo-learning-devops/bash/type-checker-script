#!/bin/bash
#return 1 if object is file, and 2 if object is folder

path="test-folder"
#path="test-file"

if [ -f "$path" ]
then
echo "1"
elif [ -d "$path" ]
then
echo "2"
fi
